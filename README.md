# MetadataTracker

[[_TOC_]] 

## 📝 Overview

This repository contains the application which tries to detect changes on data resources and describes them using PROV-O and metadata extraction methods.

## Usage

Requires [MetadataExtractorCron](https://git.rwth-aachen.de/coscine/backend/scripts/metadataextractorcron) to be run first.

Add the resources which you want to perform this application on to the "MetadataExtraction" table.

## Build

Usage of `dotnet build` to build the project. If you want to generate new api-clients, you can use the provided `sh` files.

## Possible Cases

These are the possible cases and the expected behavior of the MetadataTracker:

| Operation | With Coscine | With S3 | Run MetadataExtractor Before | No MetadataExtractor Before | Expected Behaviour |
| ------ | ------ | ------ | ------ | ------ | ------ |
| Create | x |  | x |  | Nothing (maybe a detected variant) |
| Create | x |  |  | x | Nothing (skipped) |
| Create |  | x | x |  | Nothing (Created on MetadataExtractor) |
| Create |  | x |  | x  | Metadataset Created (maybe a detected variant) |
| Update File | x |  | x |  | Metadataset Updated (maybe a detected variant) |
| Update File | x |  |  | x  | Metadataset Updated (maybe a detected variant) |
| Update File |  | x | x |  | Metadataset Updated (maybe a detected variant) |
| Update File |  | x |  | x  | Metadataset Updated (maybe a detected variant) |
| Update Metadata | x |  | x |  | Nothing (maybe a detected variant) |
| Update Metadata | x |  |  | x  | Nothing (skipped) |
| Update Metadata |  | x | x |  | Not applicable |
| Update Metadata |  | x |  | x  | Not applicable |
| Rename | x |  | x |  | Nothing |
| Rename | x |  |  | x  | Nothing |
| Rename |  | x | x |  | Detected variant + invalidation |
| Rename |  | x |  | x  | Metadataset Created + detected variant + invalidation |
| Deletion | x |  | x |  | Nothing |
| Deletion | x |  |  | x  | Nothing |
| Deletion |  | x | x |  | Invalidation |
| Deletion |  | x |  | x  | Invalidation |

## ⚙️ Configuration

The deployment script uses a configuration class `MetadataTrackerConfiguration` to manage settings such as:
- `IsEnabled`: Toggles the tracker on or off.
- `MetadataExtractionUrl`: Specifies the metadata extraction service URL.
- `SemanticSimilarityServiceUrl`: Specifies the semantic similarity service URL.
- `DetectionByteLimit`: Specifies limit the metadata extraction cronjob should adhere to.
- `Logger` Configuration: Specifies the logging level and output directory.

### Example `appsettings.json`
```json
{
  "MetadataTrackerConfiguration": {
    "IsEnabled": true,
    "MetadataExtractionUrl": "https://metadataextractor.otc.coscine.dev/",
    "SemanticSimilarityServiceUrl": "https://semanticsimilarity.metadataextractor.otc.coscine.dev/",
    "DetectionByteLimit": 16000000,
    "Logger": {
      "LogLevel": "Trace",
      "LogHome": "C:\\coscine\\logs\\MetadataTracker\\"
    }
  }
}
```

## 👥 Contributing

As an open source platform and project, we welcome contributions from our community in any form. You can do so by submitting bug reports or feature requests, or by directly contributing to Coscine's source code. To submit your contribution please follow our [Contributing Guideline](https://git.rwth-aachen.de/coscine/docs/public/wiki/-/blob/master/Contributing%20To%20Coscine.md).

## 📄 License

The current open source repository is licensed under the **MIT License**, which is a permissive license that allows the software to be used, modified, and distributed for both commercial and non-commercial purposes, with limited restrictions (see `LICENSE` file)

> The MIT License allows for free use, modification, and distribution of the software and its associated documentation, subject to certain conditions. The license requires that the copyright notice and permission notice be included in all copies or substantial portions of the software. The software is provided "as is" without any warranties, and the authors or copyright holders cannot be held liable for any damages or other liability arising from its use.

## 🆘 Support

1. **Check the documentation**: Before reaching out for support, check the help pages provided by the team at https://docs.coscine.de/en/. This may have the information you need to solve the issue.
2. **Contact the team**: If the documentation does not help you or if you have a specific question, you can reach out to our support team at `servicedesk@itc.rwth-aachen.de` 📧. Provide a detailed description of the issue you're facing, including any error messages or screenshots if applicable.
3. **Be patient**: Our team will do their best to provide you with the support you need, but keep in mind that they may have a lot of requests to handle. Be patient and wait for their response.
4. **Provide feedback**: If the support provided by our support team helps you solve the issue, let us know! This will help us improve our documentation and support processes for future users.

By following these simple steps, you can get the support you need to use Coscine's services as an external user.

## 📦 Release & Changelog

External users can find the _Releases and Changelog_ inside each project's repository. The repository contains a section for Releases (`Deployments > Releases`), where users can find the latest release changelog and source. Withing the Changelog you can find a list of all the changes made in that particular release and version.  
By regularly checking for new releases and changes in the Changelog, you can stay up-to-date with the latest improvements and bug fixes by our team and community!
