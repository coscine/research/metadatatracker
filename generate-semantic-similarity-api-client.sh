#!/bin/bash

# --------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Generate C# API Client using OpenAPI Generator:
# --------------------------------------------------------------------------------------------------------------------------------------------------------------------
# This script generates a C# API client based on the OpenAPI specification.
#
# The actions performed are:
# - Downloading the OpenAPI generator CLI tool.
# - Generating the API client code.
# - Deleting existing API client source files.
# - Copying the newly generated API client source to the project directory.
# - Replacing the README.md file.
# - Cleaning up temporary files and the downloaded tool.
#
# Defined variables:
GENERATOR_URL="https://repo1.maven.org/maven2/org/openapitools/openapi-generator-cli/7.0.1/openapi-generator-cli-7.0.1.jar" # URL to the OpenAPI generator JAR file
GENERATOR_JAR="openapi-generator-cli.jar"             # The name of the OpenAPI generator JAR file
OUTPUT_DIR="temp"                                     # The temporary directory for generated files
ARTIFACT_ID="Coscine.SemanticSimilarity.Core"                  # The artifact ID for the API client
PACKAGE_NAME="Coscine.SemanticSimilarity.Core"                 # The package name for the API client
API_SPEC_URL="https://semanticsimilarity.metadataextractor.otc.coscine.dev/swagger.json" # URL to the OpenAPI spec file
# --------------------------------------------------------------------------------------------------------------------------------------------------------------------

# ANSI color codes for styling
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color

# Download the OpenAPI generator JAR file
echo -e "${CYAN}Downloading the OpenAPI generator JAR file...${NC}"
curl -o "$GENERATOR_JAR" "$GENERATOR_URL"

# Run the OpenAPI generator
echo -e "${CYAN}Running the OpenAPI generator...${NC}"
java -jar "$GENERATOR_JAR" generate -i "$API_SPEC_URL" -g csharp -o "$OUTPUT_DIR" --artifact-id "$ARTIFACT_ID" --package-name "$PACKAGE_NAME" --skip-validate-spec --server-variables=host=localhost:7206,basePath=coscine

echo -e "${GREEN}API client generation complete.${NC}"

# Delete the current API client source
echo -e "${YELLOW}Deleting current API client source...${NC}"
rm -rf "src/$PACKAGE_NAME"
rm -rf "src/${PACKAGE_NAME}.Test"

# Copy the generated API client source to the src directory
echo -e "${CYAN}Copying generated API client source to src directory...${NC}"
cp -R "$OUTPUT_DIR/src/$PACKAGE_NAME" "src/$PACKAGE_NAME"
cp -R "$OUTPUT_DIR/src/${PACKAGE_NAME}.Test" "src/${PACKAGE_NAME}.Test"

# Remove the temp directory and the generator JAR file
echo -e "${YELLOW}Cleaning up...${NC}"
rm -rf "$OUTPUT_DIR"
rm -f "$GENERATOR_JAR"

echo -e "${GREEN}Finished.${NC}"
