﻿namespace MetadataTracker;

public class MetadataIdGenerator : IMetadataIdGenerator
{
    private const string purlPrefix = "https://purl.org/coscine/resources";

    public Uri GenerateId(string generalIdentifier, string path)
    {
        return new Uri($"{purlPrefix}/{generalIdentifier}/{path}");
    }

    public Uri GenerateDataId(string generalIdentifier, string path)
    {
        return new Uri($"{purlPrefix}/{generalIdentifier}/{path}/@type=data");
    }

    public Uri GenerateDataVersionId(string generalIdentifier, string path, long? version)
    {
        return new Uri($"{purlPrefix}/{generalIdentifier}/{path}/@type=data&version={version ?? 0}");
    }

    public Uri GenerateDataVersionExtractedId(string generalIdentifier, string path, long? version)
    {
        return new Uri($"{purlPrefix}/{generalIdentifier}/{path}/@type=data&version={version ?? 0}&extracted=true");
    }

    public Uri GenerateDataVersionHashId(string generalIdentifier, string path, long? version)
    {
        return new Uri($"{purlPrefix}/{generalIdentifier}/{path}/@type=data&version={version ?? 0}&hash={Guid.NewGuid()}");
    }

    public Uri GenerateMetadataId(string generalIdentifier, string path)
    {
        return new Uri($"{purlPrefix}/{generalIdentifier}/{path}/@type=metadata");
    }

    public Uri GenerateMetadataVersionId(string generalIdentifier, string path, long? version)
    {
        return new Uri($"{purlPrefix}/{generalIdentifier}/{path}/@type=metadata&version={version ?? 0}");
    }

    public Uri GenerateMetadataVersionExtractedId(string generalIdentifier, string path, long? version)
    {
        return new Uri($"{purlPrefix}/{generalIdentifier}/{path}/@type=metadata&version={version ?? 0}&extracted=true");
    }
}