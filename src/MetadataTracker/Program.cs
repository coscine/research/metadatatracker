﻿using MetadataTracker.Models.ConfigurationModels;
using Microsoft.Extensions.Configuration;
using NLog;
using Winton.Extensions.Configuration.Consul;

var configBuilder = new ConfigurationBuilder();

var consulUrl = Environment.GetEnvironmentVariable("CONSUL_URL") ?? "http://localhost:8500";

var configuration = configBuilder
    .AddConsul(
        "coscine/Coscine.Infrastructure/MetadataTracker/appsettings",
        options =>
        {
            options.ConsulConfigurationOptions =
                cco => cco.Address = new Uri(consulUrl);
            options.Optional = true;
            options.ReloadOnChange = true;
            options.PollWaitTime = TimeSpan.FromSeconds(5);
            options.OnLoadException = exceptionContext => exceptionContext.Ignore = true;
        }
    )
    .AddEnvironmentVariables()
    .Build();

var metadataTrackerConfiguration = new MetadataTrackerConfiguration();
configuration.Bind(MetadataTrackerConfiguration.Section, metadataTrackerConfiguration);

// Set the default LogLevel
LogManager.Configuration.Variables["logLevel"] = metadataTrackerConfiguration.Logger?.LogLevel ?? "Trace";
// Set the log location
LogManager.Configuration.Variables["logHome"] = metadataTrackerConfiguration.Logger?.LogHome ?? Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Logs");

var logger = LogManager.GetCurrentClassLogger();

if (!metadataTrackerConfiguration.IsEnabled)
{
    logger.Info("Disabled, skipped run.");
    return;
}

var tracker = new MetadataTracker.MetadataTracker(metadataTrackerConfiguration, new HttpClient());
await tracker.Track();

logger.Info("Finished successfully.");
