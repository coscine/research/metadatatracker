﻿namespace MetadataTracker;

public interface IMetadataIdGenerator
{
    Uri GenerateId(string generalIdentifier, string path);

    Uri GenerateDataId(string generalIdentifier, string path);

    Uri GenerateDataVersionId(string generalIdentifier, string path, long? version);

    Uri GenerateDataVersionExtractedId(string generalIdentifier, string path, long? version);

    Uri GenerateDataVersionHashId(string generalIdentifier, string path, long? version);

    Uri GenerateMetadataId(string generalIdentifier, string path);

    Uri GenerateMetadataVersionId(string generalIdentifier, string path, long? version);

    Uri GenerateMetadataVersionExtractedId(string generalIdentifier, string path, long? version);
}