﻿using Coscine.ApiClient.Core.Model;
using MetadataTracker.ChangeDetectors;
using MetadataTracker.DataProvider;
using MetadataTracker.MetadataStore;
using MetadataTracker.Models.ConfigurationModels;
using MetadataTracker.Services;
using MetadataTracker.Util;
using MetadataTracker.Utils;
using NLog;
using VDS.RDF;

namespace MetadataTracker;

public class MetadataTracker : IMetadataTracker
{
    private readonly MetadataTrackerConfiguration _configuration;

    private readonly IMetadataExtractorService _metadataExtractorService;
    private readonly IMetadataIdGenerator _metadataIdGenerator;
    private readonly IMetadataStore _metadataStore;
    private readonly IDataProvider _dataProvider;
    private readonly IAsyncEnumerable<IChangeDetector> _changeDetectors;

    private readonly Logger _logger = LogManager.GetCurrentClassLogger();

    public MetadataTracker(MetadataTrackerConfiguration configuration, HttpClient httpClient) : this(
        configuration,
        httpClient,
        new CoscineMetadataStore(),
        new MetadataIdGenerator())
    {
    }

    public MetadataTracker(MetadataTrackerConfiguration configuration, HttpClient httpClient, IMetadataStore metadataStore, IMetadataIdGenerator metadataIdGenerator) : this(
        configuration,
        metadataStore,
        metadataIdGenerator,
        [new HashChangeDetector(), new SimilarityChangeDetector(configuration.SemanticSimilarityServiceUrl)],
        new MetadataExtractorService(httpClient, configuration.MetadataExtractionUrl),
        new CoscineDataProvider())
    {
    }

    public MetadataTracker(MetadataTrackerConfiguration configuration, IMetadataStore metaStore, IMetadataIdGenerator metadataIdGenerator, IEnumerable<IChangeDetector> changeDetectors, IMetadataExtractorService metadataExtractorService, IDataProvider dataProvider)
    {
        _configuration = configuration;

        _metadataStore = metaStore;
        _metadataIdGenerator = metadataIdGenerator;
        _changeDetectors = changeDetectors.ToAsyncEnumerable();
        _metadataExtractorService = metadataExtractorService;
        _dataProvider = dataProvider;

        System.Net.ServicePointManager.DefaultConnectionLimit = int.MaxValue;
    }

    public async Task Track()
    {
        var currentResourceData = await VersionDetection().ToListAsync();

        await VariantDetection(currentResourceData);

        await MetadataInvalidation();
    }

    /// <summary>
    /// This method detects if a new version of research data has asynchronously created.
    /// </summary>
    /// <returns>List of resources</returns>
    private IAsyncEnumerable<ResourceData> VersionDetection()
    {
        return _dataProvider.HandleDataEntries(async (dataEntry, RetrieveFile) =>
        {
            if (dataEntry.Size > _configuration.DetectionByteLimit)
            {
                _logger.Info("Skipping {key} on {identifier} since it has a too large byte size", dataEntry.Path, dataEntry.ResourceId);
                return;
            }

            _logger.Info("Working on {key}", dataEntry.Path);

            var metadataTreeDto = await _metadataStore.GetSpecificInformationAsync(dataEntry);
            dataEntry.MetadataTreeDto = metadataTreeDto;

            var changeIndications = await _changeDetectors.SelectAwait(async (changeDetector) =>
                    await changeDetector.IsChangeDetectedAsync(dataEntry, metadataTreeDto, RetrieveFile)).ToListAsync();
            if (changeIndications.Any(x => x))
            {
                try
                {
                    var newVersion = VersionUtil.GetNewVersion();

                    var newMetadataId = _metadataIdGenerator.GenerateMetadataVersionId(dataEntry.ResourceId.ToString(), dataEntry.Path, newVersion);

                    var dataExtractedId = _metadataIdGenerator.GenerateDataVersionExtractedId(dataEntry.ResourceId.ToString(), dataEntry.Path, newVersion);
                    var metadataExtractedId = _metadataIdGenerator.GenerateMetadataVersionExtractedId(dataEntry.ResourceId.ToString(), dataEntry.Path, newVersion);

                    var metadataExtractorVersion = await _metadataExtractorService.GetVersion();
                    var metadataEntry = await _metadataExtractorService.Extract(dataEntry, RetrieveFile);

                    var tripleStore = new TripleStore();
                    if (metadataEntry.Parser != null)
                    {
                        tripleStore.LoadFromString(metadataEntry.ExtractedMetadata, metadataEntry.Parser);
                    }
                    else
                    {
                        tripleStore.LoadFromString(metadataEntry.ExtractedMetadata);
                    }
                    GraphUtil.FormatResultMetadata(tripleStore, dataExtractedId, metadataExtractedId);

                    // Set provenance information
                    var provenance = new ProvenanceParametersDto(
                        metadataExtractorVersion: metadataExtractorVersion,
                        hashParameters: new HashParametersDto("SHA512", "0"),
                        wasRevisionOf: [],
                        variants: []
                    );
                    await _changeDetectors.ForEachAwaitAsync(async (changeDetector) => await changeDetector.SetChangeDefinition(
                        dataEntry,
                        provenance,
                        metadataTreeDto,
                        tripleStore,
                        newVersion
                    ));

                    var recentMetadataVersion = metadataTreeDto?.Id;
                    if (recentMetadataVersion is null)
                    {
                        await _metadataStore.CreateExtractedMetadataAsync(dataEntry, newMetadataId, tripleStore, provenance);
                    }
                    else
                    {
                        await _metadataStore.UpdateExtractedMetadataAsync(dataEntry, newMetadataId, tripleStore, provenance);
                    }

                    _logger.Info("Described change for {key}", dataEntry.Path);

                    dataEntry.MetadataTreeDto = await _metadataStore.GetSpecificInformationAsync(dataEntry);
                }
                catch (Exception e)
                {
                    _logger.Info("Error describing the change for {key} on {identifier} with error message: {error}, Inner: {inner}",
                        dataEntry.Path, dataEntry.ResourceId, e.Message, e.InnerException != null ? e.InnerException.Message : "none");
                }
            }
            else
            {
                _logger.Info("No change for {key}", dataEntry.Path);
            }
        });
    }

    /// <summary>
    /// This method detects if an entry is a variant (closely related) to another entry.
    /// </summary>
    private async Task VariantDetection(List<ResourceData>? resourceData)
    {
        if (resourceData is null)
        {
            return;
        }

        foreach (var resourceEntry in resourceData)
        {
            if (resourceEntry.Entries is null)
            {
                continue;
            }

            var resourceDataEntries = await resourceEntry.Entries.ToListAsync();
            // Only include files
            resourceDataEntries = resourceDataEntries.Where(x => x.Type == TreeDataType.Leaf).ToList();

            // Include the metadata that belongs to removed files
            var resourceMetadataList = await _metadataStore.ListMetadata(resourceEntry, includeExtracted: true, includeProvenance: true);
            foreach (var resourceMetadata in resourceMetadataList.Where((entry) => entry is not null).Cast<MetadataTreeDto>())
            {
                if (!resourceDataEntries.Any((entry) => entry.Path == resourceMetadata.Path))
                {
                    resourceDataEntries.Add(new DataEntry(
                        new FileTreeDto(
                            resourceMetadata.Path,
                            TreeDataType.Leaf,
                            size: 0
                        ),
                        resourceEntry.ProjectId,
                        new Guid(resourceEntry.Id),
                        metadataTreeDto: resourceMetadata
                    ));
                }
            }

            _logger.Info("Create extraction graphs");
            // Get them all beforehand to avoid a O(n^2) complexity
            var extractionGraphs = new Dictionary<DataEntry, IGraph?>();
            foreach (var dataEntry in resourceDataEntries)
            {
                _logger.Info("Create extraction graph for {resourceid} {key}", dataEntry.ResourceId, dataEntry.Path);
                var extractedGraph = GetExtractionGraph(dataEntry);
                extractionGraphs.Add(dataEntry, extractedGraph);
            }

            foreach (var dataEntry in resourceDataEntries)
            {
                // Variant checking has already been performed
                if (dataEntry.MetadataTreeDto is null || dataEntry.MetadataTreeDto.Provenance?.Variants.Any() != false)
                {
                    _logger.Info("Broken or already applied variant detection for {resourceid} {key}", dataEntry.ResourceId, dataEntry.Path);
                    continue;
                }

                var recentExtractedGraph = extractionGraphs[dataEntry];

                _logger.Info("Determining variant for {resourceid} {key}", dataEntry.ResourceId, dataEntry.Path);

                var variants = new List<(DataEntry, decimal)>();

                if (recentExtractedGraph is not null)
                {
                    foreach (var otherDataEntry in resourceDataEntries.Where((entry) => entry.Path != dataEntry.Path))
                    {
                        var otherExtractedGraph = extractionGraphs[otherDataEntry];
                        if (otherExtractedGraph is null)
                        {
                            continue;
                        }

                        decimal? similarityValue = null;
                        try
                        {
                            similarityValue = await SimilarityUtil.ApplySemanticSimilarity(_configuration.SemanticSimilarityServiceUrl,
                            [
                                recentExtractedGraph,
                                otherExtractedGraph
                            ]);
                        }
                        catch (Exception e)
                        {
                            _logger.Info("Exception for similarity with {resourceid} {key}: {message}", otherDataEntry.ResourceId, otherDataEntry.Path, e);
                        }

                        // Threshold is set to 0.9 (very close)
                        if (similarityValue is not null && similarityValue >= 0.9m)
                        {
                            variants.Add((otherDataEntry, similarityValue.Value));
                        }
                    }
                }

                var isSet = false;
                var nullVariant = "https://purl.org/coscine/terms/metadatatracker#null";

                foreach (var (variant, similarity) in variants)
                {
                    _logger.Info("Assigning Variant {candidate}", variant.Path);

                    dataEntry.MetadataTreeDto?.Provenance.Variants.Add(new VariantDto()
                    {
                        GraphName = variant.MetadataTreeDto?.Id ?? nullVariant,
                        Similarity = (double)similarity
                    });
                    isSet = true;
                }

                if (!isSet)
                {
                    dataEntry.MetadataTreeDto?.Provenance.Variants.Add(new VariantDto()
                    {
                        GraphName = nullVariant,
                        Similarity = 0
                    });
                    _logger.Info("No Variant");
                }

                if (dataEntry.MetadataTreeDto?.Provenance is not null)
                {
                    await _metadataStore.UpdateProvenance(dataEntry, dataEntry.MetadataTreeDto.Provenance);
                }
                else
                {
                    _logger.Info("Issue with provenance object of {resourceid} {key}", dataEntry.ResourceId, dataEntry.Path);
                }
            }
        }
    }

    private IGraph? GetExtractionGraph(DataEntry? dataEntry)
    {
        if (dataEntry is null || dataEntry.MetadataTreeDto is null || dataEntry.MetadataTreeDto.Extracted is null)
        {
            return null;
        }

        var extractedContent = dataEntry.MetadataTreeDto.Extracted.Definition.Content;
        var format = dataEntry.MetadataTreeDto.Extracted.Definition.Type;

        var extractedMetadata = new Graph();
        if (extractedContent is not null && format is not null)
        {
            var rdfParser = MimeTypesHelper.GetParser(format);
            rdfParser.Load(extractedMetadata, new StringReader(extractedContent));
        }
        else
        {
            return null;
        }

        return extractedMetadata;
    }

    /// <summary>
    /// This method invalidates metadata that has been deleted in an external resource.
    /// </summary>
    private async Task MetadataInvalidation()
    {
        var allAsyncDataEntries = _dataProvider.HandleDataEntries(async (_, _) => await Task.CompletedTask, true);

        var resourceData = await allAsyncDataEntries.ToListAsync();

        foreach (var resourceEntry in resourceData.Where((x) => !x.Broken))
        {
            if (resourceEntry.Entries is null)
            {
                continue;
            }
            var resourceDataEntries = await resourceEntry.Entries.ToListAsync();

            var resourceMetadataList = await _metadataStore.ListMetadata(resourceEntry, includeExtracted: false, includeProvenance: false);

            foreach (var metadata in resourceMetadataList.Where((entry) => entry is not null).Cast<MetadataTreeDto>())
            {
                // If does not contain metadata entry in data list, means it is deleted (invalidated)
                if (!resourceDataEntries.Any(x => x.Path == metadata.Path))
                {
                    _logger.Info("Invalidate: {entry}", metadata.Id);
                    await _metadataStore.InvalidateMetadata(resourceEntry, metadata);
                }
            }
        }
    }
}