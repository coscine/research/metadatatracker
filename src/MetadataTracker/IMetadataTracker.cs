﻿namespace MetadataTracker;

// RQ1
public interface IMetadataTracker
{
    Task Track();
}