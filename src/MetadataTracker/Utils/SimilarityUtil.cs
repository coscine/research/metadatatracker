﻿using Coscine.SemanticSimilarity.Core.Api;
using Coscine.SemanticSimilarity.Core.Model;
using VDS.RDF;
using VDS.RDF.Writing;

namespace MetadataTracker.Utils
{
    public static class SimilarityUtil
    {
        public static string Method { get; set; } = "FilterStructureSimplerJaccardSimilarity";

        public static async Task<decimal?> ApplySemanticSimilarity(string semanticSimilarityServiceUrl, List<IGraph> extractedGraphs)
        {
            var apiClient = new DefaultApi(semanticSimilarityServiceUrl);
            var turtleWriter = new CompressingTurtleWriter();

            var similarityInput = new SimilarityInput()
            {
                Graphs = extractedGraphs.ConvertAll((graph) => new DefinitionObject()
                {
                    Definition = VDS.RDF.Writing.StringWriter.Write(graph, turtleWriter),
                    Type = "turtle"
                }),
                Methods = [Method]
            };

            var results = await apiClient.PostSemanticSimilarityWorkerAsync(similarityInput);
            var result = results.Find((entry) => entry.Method == Method);
            return result?.ReturnObject.SimilarityMatrix.GraphDistances.FirstOrDefault()?.LastOrDefault();
        }
    }
}
