﻿using VDS.RDF;

namespace MetadataTracker.Utils;

public static class GraphUtil
{
    public static void FormatResultMetadata(TripleStore tripleStore, Uri dataExtractGraph, Uri metadataExtractGraph)
    {
        foreach (var graph in tripleStore.Graphs.ToArray())
        {
            if (graph.Name is IUriNode)
            {
                var graphName = (IUriNode)graph.Name;
                if (graphName.Uri.AbsoluteUri != dataExtractGraph.AbsoluteUri && graphName.Uri.AbsoluteUri != metadataExtractGraph.AbsoluteUri)
                {
                    tripleStore.Remove(graph.Name);
                    if (graphName.Uri.AbsoluteUri.Contains("type=data"))
                    {
                        var newGraph = new Graph(dataExtractGraph)
                        {
                            BaseUri = dataExtractGraph
                        };
                        newGraph.Assert(graph.Triples);
                        tripleStore.Add(newGraph, true);
                    }
                    else
                    {
                        var newGraph = new Graph(metadataExtractGraph)
                        {
                            BaseUri = metadataExtractGraph
                        };
                        newGraph.Assert(graph.Triples);
                        tripleStore.Add(newGraph, true);
                    }
                }
            }
        }
    }
}
