﻿using Coscine.ApiClient.Core.Model;

namespace MetadataTracker.DataProvider;

public class MockupDataProvider : IDataProvider
{
    private readonly string _path;

    public MockupDataProvider() : this(".")
    {
    }

    public MockupDataProvider(string path)
    {
        _path = path;
    }

    public async IAsyncEnumerable<ResourceData> HandleDataEntries(Func<DataEntry, Func<Task<Stream>>, Task> useDataEntry, bool all = false)
    {
        const string resourceId = "4c9d4c7d-cf97-47c3-adaf-f511d199aa56";
        var dataEntries = new List<DataEntry>();
        foreach (var file in Directory.GetFiles(_path))
        {
            var fileInfo = new FileInfo(file);
            var dataEntry =
                new DataEntry(
                    new FileTreeDto(
                        fileInfo.Name,
                        TreeDataType.Leaf,
                        "/",
                        fileInfo.Name,
                        ".txt",
                        fileInfo.Length,
                        DateTime.Now,
                        DateTime.Now,
                        hidden: false
                    ),
                    resourceId,
                    new Guid(resourceId)
                );
            dataEntries.Add(dataEntry);
            await useDataEntry(dataEntry,
                async () => await Task.Run(() => new MemoryStream(File.ReadAllBytes(file)))
           );
        }
        yield return new ResourceData()
        {
            Entries = dataEntries.ToAsyncEnumerable(),
            Id = resourceId,
            ProjectId = resourceId,
            Broken = false,
        };
    }

    public bool IsActiveResource(string resourceId)
    {
        return true;
    }
}