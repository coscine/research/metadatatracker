﻿using Coscine.ApiClient.Core.Model;

namespace MetadataTracker.DataProvider;

public class DataEntry : FileTreeDto
{
    public string ProjectId { get; set; }
    public Guid ResourceId { get; set; }
    public IDictionary<string, string> Hashes { get; set; } = new Dictionary<string, string>();

    public MetadataTreeDto? MetadataTreeDto { get; set; }

    public DataEntry(FileTreeDto entry, string projectId, Guid resourceId, MetadataTreeDto? metadataTreeDto = null) : base(
        entry.Path,
        entry.Type,
        entry.Directory,
        entry.Name,
        entry.Extension,
        entry.Size,
        entry.CreationDate,
        entry.ChangeDate,
        entry.Actions,
        entry.Hidden
    )
    {
        ProjectId = projectId;
        ResourceId = resourceId;
        MetadataTreeDto = metadataTreeDto;
    }
}