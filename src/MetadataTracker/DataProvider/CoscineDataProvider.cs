﻿using NLog;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace MetadataTracker.DataProvider;

public class CoscineDataProvider : IDataProvider
{
    private readonly AdminApi _adminApi;
    private readonly BlobApi _blobApi;
    private readonly TreeApi _treeApi;

    private Logger _logger = LogManager.GetCurrentClassLogger();

    public CoscineDataProvider()
    {
        // Generate an admin token for the API Client
        var jwtConfiguration = ApiConfigurationUtil.RetrieveJwtConfiguration();
        var adminToken = ApiConfigurationUtil.GenerateAdminToken(jwtConfiguration);
        var apiConfiguration = new Configuration()
        {
            BasePath = "http://localhost:7206/coscine",
            ApiKeyPrefix = { { "Authorization", "Bearer" } },
            ApiKey = { { "Authorization", adminToken } },
            Timeout = 300000, // 5 minutes
        };
        _adminApi = new AdminApi(apiConfiguration);
        _blobApi = new BlobApi(apiConfiguration);
        _treeApi = new TreeApi(apiConfiguration);
    }

    public async IAsyncEnumerable<ResourceData> HandleDataEntries(Func<DataEntry, Func<Task<Stream>>, Task> useDataEntry, bool all = false)
    {
        var resourcesAsyncList = PaginationHelper.GetAllAsync<ResourceAdminDtoPagedResponse, ResourceAdminDto>(
            (currentPage) => _adminApi.GetAllResourcesAsync(includeDeleted: false, pageNumber: currentPage, pageSize: 50)
        );

        await foreach (var resource in resourcesAsyncList)
        {
            var projectId = resource.ProjectResources.FirstOrDefault()?.ProjectId;
            if ((!resource.MetadataExtraction && !all) || !projectId.HasValue)
            {
                continue;
            }
            IAsyncEnumerable<DataEntry>? entries = null;
            var broken = false;
            try
            {
                entries = HandleFolder("", projectId.Value, resource, useDataEntry);
            }
            catch (Exception e)
            {
                _logger.Error("Error on resource {id} {name} with message {error}",
                    resource.Id,
                    resource.DisplayName,
                    e
                );
                broken = true;
            }
            yield return new ResourceData()
            {
                Id = resource.Id.ToString(),
                ProjectId = projectId.Value.ToString(),
                Entries = entries,
                Broken = broken,
            };
        }
    }

    private async IAsyncEnumerable<DataEntry> HandleFolder(string folder, Guid projectId, ResourceAdminDto resource, Func<DataEntry, Func<Task<Stream>>, Task> useDataEntry)
    {
        IEnumerable<FileTreeDto>? fileInfos = null;
        try
        {
            fileInfos = await PaginationHelper.GetAll<FileTreeDtoPagedResponse, FileTreeDto>(
                (currentPage) => _treeApi.GetFileTreeAsync(projectId.ToString(), resource.Id, folder, pageNumber: currentPage, pageSize: 50)
            );
        }
        catch (Exception e)
        {
            _logger.Error("Exception on resource {id} {name} with message {error}",
                resource.Id,
                resource.DisplayName,
                e
            );
        }
        if (fileInfos is null)
        {
            _logger.Error("Error on resource {id} {displayName} on path '{folder}'",
                resource.Id,
                resource.DisplayName,
                folder
            );
            yield break;
        }

        _logger.Info("There are {count} entries for resource {id} {displayName} on path '{folder}'",
            fileInfos.Count(),
            resource.Id,
            resource.DisplayName,
            folder
        );

        foreach (var fileInfo in fileInfos)
        {
            // Ignore .coscine folder
            if (fileInfo.Type == TreeDataType.Tree && fileInfo.Hidden == false)
            {
                await foreach (var entry in HandleFolder(fileInfo.Path, projectId, resource, useDataEntry))
                {
                    yield return entry;
                }
                // Add folders
                var dataEntry = new DataEntry(fileInfo, projectId.ToString(), resource.Id);
                yield return dataEntry;
            }
            else if (fileInfo.Hidden == false)
            {
                var dataEntry = new DataEntry(fileInfo, projectId.ToString(), resource.Id);
                yield return dataEntry;
                await useDataEntry(dataEntry, async () =>
                {
                    return await _blobApi.GetBlobAsync(projectId.ToString(), resource.Id, fileInfo.Path)
                        ?? throw new NullReferenceException("The resulting stream of the loaded entry is null.");
                });
            }
        }
    }
}