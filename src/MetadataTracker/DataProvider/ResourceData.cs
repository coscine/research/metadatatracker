﻿namespace MetadataTracker.DataProvider
{
    public class ResourceData
    {
        public string Id { get; set; } = null!;
        public string ProjectId { get; set; } = null!;
        public IAsyncEnumerable<DataEntry>? Entries { get; set; }
        public bool Broken { get; set; }
    }
}
