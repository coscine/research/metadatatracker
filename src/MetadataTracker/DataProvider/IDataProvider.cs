﻿namespace MetadataTracker.DataProvider;

public interface IDataProvider
{
    IAsyncEnumerable<ResourceData> HandleDataEntries(Func<DataEntry, Func<Task<Stream>>, Task> useDataEntry, bool all = false);
}
