﻿using VDS.RDF;

namespace MetadataTracker.Services;

public class MetadataEntry
{
    public string? ExtractedMetadata;
    public IStoreReader? Parser;
}