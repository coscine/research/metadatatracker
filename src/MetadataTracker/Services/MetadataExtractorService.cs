﻿using MetadataTracker.DataProvider;
using System.Globalization;
using VDS.RDF.Parsing;
using Coscine.MetadataExtractor.Core.Api;

namespace MetadataTracker.Services;

public class MetadataExtractorService : IMetadataExtractorService
{
    private readonly HttpClient HttpClient;

    private readonly DefaultApi _apiClient;

    public MetadataExtractorService() : this(new HttpClient())
    {
    }

    public MetadataExtractorService(HttpClient httpClient) : this(httpClient, "https://metadataextractor.otc.coscine.dev/")
    {
    }

    /// <summary>
    /// Sets HttpClient Timeout to 30 minutes
    /// </summary>
    /// <param name="httpClient"></param>
    /// <param name="metadataExtractionServiceUrl"></param>
    public MetadataExtractorService(HttpClient httpClient, string metadataExtractionServiceUrl)
    {
        HttpClient = httpClient;
        HttpClient.Timeout = TimeSpan.FromMinutes(30);

        _apiClient = new DefaultApi(metadataExtractionServiceUrl);
    }

    public async Task<MetadataEntry> Extract(DataEntry entry, Func<Task<Stream>> RetrieveFile)
    {
        var loadedEntry = await RetrieveFile();
        if (loadedEntry is null)
        {
            throw new NullReferenceException("The resulting stream of the loaded entry is null.");
        }

        var tempFile = Path.Combine(Path.GetTempPath(), Path.GetFileName(entry.Path));
        using (var fs = new FileStream(tempFile, FileMode.Create, FileAccess.Write))
        {
            loadedEntry.CopyTo(fs);
        }

        var givenStream = File.OpenRead(tempFile);
        try
        {
            var resultList = await _apiClient.PostMetadataExtractorWorkerAsync(
                "application/json",
                identifier: $"{entry.ResourceId}/{entry.Path.Replace("\\", "/")}",
                creationDate: entry.CreationDate?.ToString("o", CultureInfo.InvariantCulture)!,
                modificationDate: entry.ChangeDate?.ToString("o", CultureInfo.InvariantCulture)!,
                file: givenStream
            );
            var result = resultList[0];

            return new MetadataEntry()
            {
                ExtractedMetadata = result.Metadata,
                Parser = new TriGParser(TriGSyntax.Rdf11)
            };
        }
        finally
        {
            givenStream.Dispose();
            File.Delete(tempFile);
        }
    }

    public async Task<string> GetVersion()
    {
        var resultList = await _apiClient.GetVersionWorkerAsync();
        return resultList.VarVersion;
    }
}