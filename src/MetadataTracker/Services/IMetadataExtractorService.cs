﻿using MetadataTracker.DataProvider;

namespace MetadataTracker.Services;

// RQ2
public interface IMetadataExtractorService
{
    Task<MetadataEntry> Extract(DataEntry entry, Func<Task<Stream>> RetrieveFile);
    Task<string> GetVersion();
}