﻿using Coscine.ApiClient.Core.Model;
using MetadataTracker.DataProvider;
using VDS.RDF;

namespace MetadataTracker.MetadataStore;

public interface IMetadataStore
{
    Task<MetadataTreeDto?> GetSpecificInformationAsync(DataEntry dataEntry, int? version = null);
    Task CreateExtractedMetadataAsync(DataEntry dataEntry, Uri newMetadataId, TripleStore extractedMetadata, ProvenanceParametersDto provenance);
    Task UpdateExtractedMetadataAsync(DataEntry dataEntry, Uri newMetadataId, TripleStore extractedMetadata, ProvenanceParametersDto provenance);
    Task UpdateProvenance(DataEntry dataEntry, ProvenanceDto provenance);
    Task<IEnumerable<MetadataTreeDto?>> ListMetadata(ResourceData resourceEntry, bool includeExtracted, bool includeProvenance);
    Task InvalidateMetadata(ResourceData resourceEntry, MetadataTreeDto metadata);
}