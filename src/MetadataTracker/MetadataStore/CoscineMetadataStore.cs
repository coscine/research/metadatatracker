﻿using Coscine.ApiClient;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;
using MetadataTracker.DataProvider;
using VDS.RDF;
using VDS.RDF.Writing;

namespace MetadataTracker.MetadataStore;

public class CoscineMetadataStore : IMetadataStore
{
    private readonly ProvenanceApi _provenanceApi;
    private readonly TreeApi _treeApi;

    public CoscineMetadataStore()
    {
        // Generate an admin token for the API Client
        var jwtConfiguration = ApiConfigurationUtil.RetrieveJwtConfiguration();
        var adminToken = ApiConfigurationUtil.GenerateAdminToken(jwtConfiguration);
        var apiConfiguration = new Configuration()
        {
            BasePath = "http://localhost:7206/coscine",
            ApiKeyPrefix = { { "Authorization", "Bearer" } },
            ApiKey = { { "Authorization", adminToken } },
            Timeout = 300000, // 5 minutes
        };
        _provenanceApi = new ProvenanceApi(apiConfiguration);
        _treeApi = new TreeApi(apiConfiguration);
    }

    public async Task<MetadataTreeDto?> GetSpecificInformationAsync(DataEntry dataEntry, int? version = null)
    {
        var response = await _treeApi.GetSpecificMetadataTreeAsync(
            dataEntry.ProjectId,
            dataEntry.ResourceId,
            dataEntry.Path,
            varVersion: version,
            includeExtractedMetadata: true,
            includeProvenance: true
        );
        return response?.Data;
    }

    public async Task CreateExtractedMetadataAsync(DataEntry dataEntry, Uri newMetadataId, TripleStore extractedMetadata, ProvenanceParametersDto provenance)
    {
        var trigWriter = new TriGWriter();
        await _treeApi.CreateExtractedMetadataTreeAsync(
            dataEntry.ProjectId,
            dataEntry.ResourceId,
            new ExtractedMetadataTreeForCreationDto(
                path: dataEntry.Path,
                id: newMetadataId.AbsoluteUri,
                definition: new RdfDefinitionForManipulationDto(
                    VDS.RDF.Writing.StringWriter.Write(extractedMetadata, trigWriter),
                    RdfFormat.ApplicationXTrig
                ),
                provenance: provenance
            )
        );
    }

    public async Task UpdateExtractedMetadataAsync(DataEntry dataEntry, Uri newMetadataId, TripleStore extractedMetadata, ProvenanceParametersDto provenance)
    {
        var trigWriter = new TriGWriter();
        await _treeApi.UpdateExtractedMetadataTreeAsync(
            dataEntry.ProjectId,
            dataEntry.ResourceId,
            new ExtractedMetadataTreeForUpdateDto(
                path: dataEntry.Path,
                id: newMetadataId.AbsoluteUri,
                definition: new RdfDefinitionForManipulationDto(
                    VDS.RDF.Writing.StringWriter.Write(extractedMetadata, trigWriter),
                    RdfFormat.ApplicationXTrig
                ),
                provenance: provenance,
                forceNewMetadataVersion: true
            )
        );
    }

    public async Task UpdateProvenance(DataEntry dataEntry, ProvenanceDto provenance)
    {
        await _provenanceApi.UpdateSpecificProvenanceAsync(
            dataEntry.ProjectId,
            dataEntry.ResourceId,
            new ProvenanceForUpdateDto(
                wasRevisionOf: provenance.WasRevisionOf,
                variants: provenance.Variants,
                wasInvalidatedBy: provenance.WasInvalidatedBy,
                similarityToLastVersion: provenance.SimilarityToLastVersion,
                hashParameters: provenance.HashParameters,
                id: provenance.Id
            )
        );
    }

    public async Task<IEnumerable<MetadataTreeDto?>> ListMetadata(ResourceData resourceEntry, bool includeExtracted, bool includeProvenance)
    {
        // TODO: Deal with metadata in folders? Recursive?
        return await PaginationHelper.GetAll<MetadataTreeDtoPagedResponse, MetadataTreeDto>(
            (currentPage) => _treeApi.GetMetadataTreeAsync(
                resourceEntry.ProjectId,
                new Guid(resourceEntry.Id),
                includeExtractedMetadata: includeExtracted,
                includeProvenance: includeProvenance,
                pageNumber: currentPage, pageSize: 50
            )
        );
    }

    public async Task InvalidateMetadata(ResourceData resourceEntry, MetadataTreeDto metadata)
    {
        await _treeApi.DeleteMetadataTreeWithHttpInfoAsync(
            resourceEntry.ProjectId,
            new Guid(resourceEntry.Id),
            new MetadataTreeForDeletionDto(
                path: metadata.Path,
                invalidatedBy: "https://purl.org/coscine/terms/metadatatracker#Agent",
                varVersion: long.Parse(metadata.VarVersion)
            )
        );
    }
}
