﻿using Coscine.ApiClient.Core.Model;
using MetadataTracker.DataProvider;
using VDS.RDF;

namespace MetadataTracker.MetadataStore;

public class MockupMetadataStore : IMetadataStore
{
    private readonly MetadataTreeDto _exampleMetadataTreeDto;

    public MockupMetadataStore()
    {
        _exampleMetadataTreeDto = new MetadataTreeDto("text.txt", TreeDataType.Leaf,
           provenance: new ProvenanceDto(id: "myId", generatedAt: DateTime.Now, [], [], 0));
    }

    public Task CreateExtractedMetadataAsync(DataEntry dataEntry, Uri newMetadataId, TripleStore extractedMetadata, ProvenanceParametersDto provenance)
    {
        return Task.CompletedTask;
    }

    public Task<MetadataTreeDto?> GetSpecificInformationAsync(DataEntry dataEntry, int? version = null)
    {
        return Task.FromResult<MetadataTreeDto?>(_exampleMetadataTreeDto);
    }

    public Task InvalidateMetadata(ResourceData resourceEntry, MetadataTreeDto metadata)
    {
        return Task.CompletedTask;
    }

    public Task<IEnumerable<MetadataTreeDto?>> ListMetadata(ResourceData resourceEntry, bool includeExtracted, bool includeProvenance)
    {
        return Task.FromResult<IEnumerable<MetadataTreeDto?>>([_exampleMetadataTreeDto]);
    }

    public Task UpdateExtractedMetadataAsync(DataEntry dataEntry, Uri newMetadataId, TripleStore extractedMetadata, ProvenanceParametersDto provenance)
    {
        return Task.CompletedTask;
    }

    public Task UpdateProvenance(DataEntry dataEntry, ProvenanceDto provenance)
    {
        return Task.CompletedTask;
    }
}
