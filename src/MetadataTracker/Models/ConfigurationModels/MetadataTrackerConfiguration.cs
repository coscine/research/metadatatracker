﻿namespace MetadataTracker.Models.ConfigurationModels;

/// <summary>
/// Represents the configuration settings used in the application.
/// </summary>
public class MetadataTrackerConfiguration
{
    /// <summary>
    /// The section name in the configuration file.
    /// </summary>
    public static readonly string Section = "MetadataTrackerConfiguration";

    /// <summary>
    /// Value indicating whether Metadata Extraction is enabled.
    /// </summary>
    public bool IsEnabled { get; init; } = true;

    /// <summary>
    /// Url of the metadata extraction service.
    /// </summary>
    public string MetadataExtractionUrl { get; init; } = "https://metadataextractor.otc.coscine.dev/";

    /// <summary>
    /// Url of the similarity service.
    /// </summary>
    public string SemanticSimilarityServiceUrl { get; init; } = "https://semanticsimilarity.metadataextractor.otc.coscine.dev/";

    /// <summary>
    /// The maximum file size for research data which should be analyzed by e.g. Metadata Extractor or Tracker
    /// </summary>
    public long DetectionByteLimit { get; init; } = 16 * 1000 * 1000L;

    /// <summary>
    /// Logger configuration settings.
    /// </summary>
    /// <value>
    /// The logger storage configuration settings, or <c>null</c> if not configured.
    /// </value>
    public LoggerConfiguration? Logger { get; init; }

    /// <summary>
    /// Represents the configuration settings for the logger.
    /// </summary>
    public record LoggerConfiguration(string? LogLevel, string? LogHome);
}
