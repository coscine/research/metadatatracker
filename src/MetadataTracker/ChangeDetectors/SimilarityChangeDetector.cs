﻿using Coscine.ApiClient.Core.Model;
using MetadataTracker.DataProvider;
using MetadataTracker.Utils;
using NLog;
using VDS.RDF;

namespace MetadataTracker.ChangeDetectors;

public class SimilarityChangeDetector : IChangeDetector
{
    private readonly string _semanticSimilarityServiceUrl;

    private Logger _logger = LogManager.GetCurrentClassLogger();

    public SimilarityChangeDetector(string semanticSimilarityServiceUrl = "https://semanticsimilarity.metadataextractor.otc.coscine.dev/")
    {
        _semanticSimilarityServiceUrl = semanticSimilarityServiceUrl;
    }

    private async Task<decimal?> GetSimilarityValue(List<IGraph> extractedGraphs)
    {
        return await SimilarityUtil.ApplySemanticSimilarity(_semanticSimilarityServiceUrl, extractedGraphs);
    }

    public async Task<bool> IsChangeDetectedAsync(DataEntry _, MetadataTreeDto? metadataTreeDto, Func<Task<Stream>> __)
    {
        if (metadataTreeDto is null)
        {
            return true;
        }
        await Task.CompletedTask;
        return false;
    }

    public async Task SetChangeDefinition(DataEntry dataEntry, ProvenanceParametersDto provenanceParameters, MetadataTreeDto? metadataTreeDto, TripleStore extractedMetadata, long version)
    {
        try
        {
            var extractedGraphs = new List<IGraph>();

            var extractedContent = metadataTreeDto?.Extracted?.Definition?.Content;
            var format = metadataTreeDto?.Extracted?.Definition?.Type;

            var oldExtractedMetadata = new Graph();
            if (extractedContent is not null && format is not null)
            {
                var rdfParser = MimeTypesHelper.GetParser(format);
                rdfParser.Load(oldExtractedMetadata, new StringReader(extractedContent));
            }
            extractedGraphs.Add(oldExtractedMetadata);

            var newExtractedGraph = new Graph();
            foreach (var extractedTriple in extractedMetadata.Triples)
            {
                newExtractedGraph.Assert(extractedTriple);
            }
            extractedGraphs.Add(newExtractedGraph);

            var similarityValue = await GetSimilarityValue(extractedGraphs);

            provenanceParameters.SimilarityToLastVersion = (double?) similarityValue;
        }
        catch (Exception e)
        {
            _logger.Error("Exception describing similarity: {message}", e);
        }
    }
}