﻿using Coscine.ApiClient.Core.Model;
using MetadataTracker.DataProvider;
using VDS.RDF;

namespace MetadataTracker.ChangeDetectors;

// RQ3
public interface IChangeDetector
{
    Task<bool> IsChangeDetectedAsync(DataEntry dataEntry, MetadataTreeDto? metadataTreeDto, Func<Task<Stream>> RetrieveFile);

    Task SetChangeDefinition(DataEntry dataEntry, ProvenanceParametersDto provenanceParameters, MetadataTreeDto? metadataTreeDto, TripleStore extractedMetadata, long version);
}