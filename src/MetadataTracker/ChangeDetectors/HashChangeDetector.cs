﻿using Coscine.ApiClient.Core.Model;
using MetadataTracker.DataProvider;
using System.Security.Cryptography;
using VDS.RDF;

namespace MetadataTracker.ChangeDetectors;

public class HashChangeDetector : IChangeDetector
{
    private static HashAlgorithm GetHashAlgorithm(System.Security.Cryptography.HashAlgorithmName hashAlgorithmName)
    {
        if (hashAlgorithmName == System.Security.Cryptography.HashAlgorithmName.MD5)
            return MD5.Create();
        if (hashAlgorithmName == System.Security.Cryptography.HashAlgorithmName.SHA1)
            return SHA1.Create();
        if (hashAlgorithmName == System.Security.Cryptography.HashAlgorithmName.SHA256)
            return SHA256.Create();
        if (hashAlgorithmName == System.Security.Cryptography.HashAlgorithmName.SHA384)
            return SHA384.Create();
        if (hashAlgorithmName == System.Security.Cryptography.HashAlgorithmName.SHA512)
            return SHA512.Create();

        throw new CryptographicException($"Unknown hash algorithm \"{hashAlgorithmName.Name}\".");
    }

    protected static byte[] HashData(Stream data,
        System.Security.Cryptography.HashAlgorithmName hashAlgorithm)
    {
        using var hashAlgorithmObject = GetHashAlgorithm(hashAlgorithm);
        return hashAlgorithmObject.ComputeHash(data);
    }

    public async Task<bool> IsChangeDetectedAsync(DataEntry dataEntry, MetadataTreeDto? metadataTreeDto, Func<Task<Stream>> RetrieveFile)
    {
        var fileStream = await RetrieveFile();
        var hashAlgorithmName = System.Security.Cryptography.HashAlgorithmName.SHA512;
        dataEntry.Hashes = new Dictionary<string, string>()
        {
            { hashAlgorithmName.Name!, Convert.ToBase64String(HashData(fileStream, hashAlgorithmName)) }
        };

        if (metadataTreeDto is null)
        {
            return true;
        }

        // If these values are not set, the metadata extractor still has to run => Skip those
        var usedHashAlgorithm = metadataTreeDto.Provenance?.HashParameters?.AlgorithmName;
        var usedHashValue = metadataTreeDto.Provenance?.HashParameters?.Value;
        if (usedHashAlgorithm is null || usedHashValue is null)
        {
            return false;
        }

        var changeDetected = false;
        foreach (var hash in dataEntry.Hashes)
        {
            var hashAlgorithm = hash.Key;
            var hashResult = hash.Value;
            if (usedHashAlgorithm == hashAlgorithm && usedHashValue != hashResult)
            {
                changeDetected = true;
                break;
            }
        }
        return changeDetected;
    }

    public async Task SetChangeDefinition(DataEntry dataEntry, ProvenanceParametersDto provenanceParameters, MetadataTreeDto? _, TripleStore __, long ___)
    {
        await Task.CompletedTask;
        foreach (var hash in dataEntry.Hashes)
        {
            provenanceParameters.HashParameters = new HashParametersDto(
                hash.Key,
                hash.Value
            );
        }
    }
}