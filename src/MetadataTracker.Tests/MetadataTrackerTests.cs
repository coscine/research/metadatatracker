﻿using MetadataTracker.ChangeDetectors;
using MetadataTracker.DataProvider;
using MetadataTracker.MetadataStore;
using MetadataTracker.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace MetadataTracker.Tests;

[TestFixture]
public class MetadataTrackerTests
{
    private IMetadataTracker _metadataTracker;
    private string tempDirectory;

    [OneTimeSetUp]
    public void Init()
    {
        tempDirectory = Path.Join(Path.GetTempPath(), Guid.NewGuid().ToString());
        Directory.CreateDirectory(tempDirectory);
        var tempFile1 = Path.Join(tempDirectory, "test.txt");
        using (var writer = File.CreateText(tempFile1))
        {
            writer.WriteLine("This is an example text for the purpose of testing.");
        }
        var tempFile2 = Path.Join(tempDirectory, "test2.txt");
        using (var writer = File.CreateText(tempFile2))
        {
            writer.WriteLine("This is an example text for the purpose of testing.");
        }

        var metadataIdGenerator = new MetadataIdGenerator();
        var metadataStore = new MockupMetadataStore();
        _metadataTracker = new MetadataTracker(
            new Models.ConfigurationModels.MetadataTrackerConfiguration(),
            metadataStore,
            metadataIdGenerator,
            new List<IChangeDetector> { new HashChangeDetector(), new SimilarityChangeDetector() },
            new MetadataExtractorService(),
            new MockupDataProvider(tempDirectory)
        );
    }

    [OneTimeTearDown]
    public void Cleanup()
    {
        Directory.Delete(tempDirectory, true);
    }

    [Test]
    public async Task MockupTest()
    {
        var task = _metadataTracker.Track();
        await task;
        Assert.That(task.IsCompletedSuccessfully);
    }
}